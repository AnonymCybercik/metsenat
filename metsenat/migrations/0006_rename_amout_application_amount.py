# Generated by Django 3.2 on 2022-09-26 06:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('metsenat', '0005_auto_20220925_2151'),
    ]

    operations = [
        migrations.RenameField(
            model_name='application',
            old_name='amout',
            new_name='amount',
        ),
    ]
