# Generated by Django 3.2 on 2022-09-26 10:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('metsenat', '0007_student_application_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='degree',
            field=models.CharField(choices=[('Bakalavr', 'Bakalavr'), ('Magister', 'Magister'), ('Doktorantura', 'Doktorantura'), ('Barchasi', 'Barchasi')], default=0, max_length=255),
        ),
        migrations.AlterField(
            model_name='student_application',
            name='status',
            field=models.CharField(choices=[('Yangi', 'Yangi'), ('Moderatsiyada', 'Moderatsiyada'), ('Tasdiqlangan', 'Tasdiqlangan'), ('Bekor Qilingan', 'Bekor Qilingan')], default=0, max_length=225),
        ),
    ]
