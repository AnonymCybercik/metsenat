from django.db import models
from django.contrib.auth.models import User


DEGREE_TYPES = [
		('Bakalavr', 'Bakalavr'),
		('Magister', 'Magister'),
		('Doktorantura', 'Doktorantura'),
		('Barchasi', 'Barchasi')
	]

APP_STATUS = [
	("Yangi", "Yangi"),
	("Moderatsiyada", "Moderatsiyada"),
	("Tasdiqlangan", "Tasdiqlangan"),
	("Bekor Qilingan", "Bekor Qilingan"),
]

class Student(models.Model):

	full_name = models.CharField(max_length = 255)
	degree = models.CharField(max_length = 255, choices = DEGREE_TYPES, default = 0)
	otm = models.CharField(max_length = 255)
	phone = models.CharField(max_length = 255)
	payment_contract = models.IntegerField()
	status = models.BooleanField(default = True)
	balance = models.IntegerField()

	created_date = models.DateTimeField(auto_now=True)
	updated_time = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.full_name
		

class Application(models.Model):

	full_name = models.CharField(max_length = 255)
	phone = models.CharField(max_length = 255)
	amount = models.IntegerField()
	is_juridical = models.BooleanField(default = False)
	organization = models.CharField(max_length = 255, null = True, blank = True)

	created_date = models.DateTimeField(auto_now=True)
	updated_time = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.full_name


class Student_application(models.Model):

	application = models.ForeignKey(Application, on_delete = models.RESTRICT)
	student = models.ForeignKey(Student, on_delete = models.RESTRICT)
	admin = models.ForeignKey(User, on_delete = models.CASCADE)
	amount = models.IntegerField()
	status = models.CharField(max_length = 225, choices = APP_STATUS, default = 0)

	created_date = models.DateTimeField(auto_now = True)
	updated_time = models.DateTimeField(auto_now_add = True)

	def __str__(self):
		return self.student.full_name
