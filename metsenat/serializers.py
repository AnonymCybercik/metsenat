from rest_framework import serializers
from .models import (
        Application, Student, Student_application
    )
from django.contrib.auth.models import User


class ApplicationListCreateSerializer(serializers.ModelSerializer):
    created_date = serializers.DateTimeField(format="%Y-%m-%d")
    updated_time = serializers.DateTimeField(format="%Y-%m-%d")

    class Meta:
        model = Application
        fields = (
            "full_name",
            "phone",
            "amount",
            "is_juridical",
            "organization",
            "created_date",
            "updated_time",
        )

class StudentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Student
        fields = (
            "full_name",
            "degree",
            "otm",
            "phone",
            "payment_contract",
            "status",
        )

class AdminSerializer(serializers.ModelSerializer):
    first_name = serializers.SerializerMethodField(read_only = True)

    class Meta:
        model = User
        fields = (
            "first_name",
        )

    def get_first_name(self, obj):
        return f"{obj.first_name} {obj.last_name}"

class StudentApplicationSerializer(serializers.ModelSerializer):
    

    class Meta:
        model = Student_application
        fields = (
            "application",
            "student",
            "admin",
            "amount",
            "created_date",
            "updated_time",
        )

class StudentApplicationListSerializer(serializers.ModelSerializer):
    application = ApplicationListCreateSerializer(read_only = True)
    student = StudentSerializer(read_only = True)
    admin = AdminSerializer(read_only = True)
    created_date = serializers.DateTimeField(format="%Y-%m-%d")
    updated_time = serializers.DateTimeField(format="%Y-%m-%d")

    class Meta:
        model = Student_application
        fields = (
            "application",
            "student",
            "admin",
            "amount",
            "created_date",
            "updated_time",
        )