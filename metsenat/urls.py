from django.urls import path, include
from .views import (
    StudentList, 
    ApplicationList, 
    StudentApplication, 
    Statistics,
    StudentDetailUpdate,
    ApplicationDetailUpdate,
    StudentApplicationDetailUpdate,
)
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    
)

urlpatterns = [
    path('student/list/', StudentList.as_view()),
    path('application/list/', ApplicationList.as_view()),
    path('student/application/list/', StudentApplication.as_view()),
    path('student/login/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('student/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

    path('application/detail/<int:pk>/', ApplicationDetailUpdate.as_view()),
    path('student/detail/<int:pk>/', StudentDetailUpdate.as_view()),
    path('student-app/detail/<int:pk>/', StudentApplicationDetailUpdate.as_view()),


    
    #statistics
    path('statistics/', Statistics.as_view()),
]