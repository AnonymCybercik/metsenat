from django.contrib import admin

from .models import (
		Student, Student_application, Application
	)

admin.site.register(Student)
admin.site.register(Student_application)
admin.site.register(Application)
