from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.generics import GenericAPIView, ListCreateAPIView
from .serializers import (
    ApplicationListCreateSerializer, StudentSerializer, StudentApplicationSerializer, StudentApplicationListSerializer
)
from .models import (
    Application, Student, Student_application
)
from django_filters import rest_framework as filters
from .filters import StudentFilter
from rest_framework.views import APIView
from django.db.models import Avg
from datetime import timedelta
from django.utils import timezone
from django.db.models import Count
from django.db.models.functions import ExtractDay


class StudentList(ListCreateAPIView):
    queryset = Student.objects.filter(status = True)
    serializer_class = StudentSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = StudentFilter

class ApplicationList(ListCreateAPIView):
    queryset = Application.objects.all()
    serializer_class = ApplicationListCreateSerializer
    

class StudentApplication(GenericAPIView):
    queryset = Student_application.objects.all()
    serializer_class = StudentApplicationSerializer

    def get(self, request):
        serializer = StudentApplicationListSerializer(self.get_queryset(), many = True)
        return Response(serializer.data)

    def post(self, request):
        user = request.user
        request.data["admin"] = user.id

        serializer = StudentApplicationSerializer(data = request.data, many = False)
        if serializer.is_valid():
            v_data = serializer.validated_data
            if int(v_data["student"].payment_contract) >= int(v_data["student"].balance) + int(v_data["amount"]):
                if (int(v_data["application"].amount) - int(v_data["amount"])) >= 0:
                    v_data["application"].amount = int(v_data["application"].amount) - int(v_data["amount"])
                    v_data["student"].balance += v_data["amount"]
                    v_data["student"].save()
                    v_data["application"].save()

                    serializer.save()
                else:
                    return Response("studentga berilgan summa xomiy summasidan ko'p", status = 400)    
            else:
                return Response("kiritilgan summa talabani summasidan katta", status = 400)
        else:
            return Response(serializer.errors)

        return Response(serializer.data, status = 201)
        

class Statistics(APIView):

    def get(self, request):
        student_balance = round(Student.objects.filter(status = True).aggregate(Avg("balance"))["balance__avg"], 0)
        student_contract = round(Student.objects.filter(status = True).aggregate(Avg("payment_contract"))["payment_contract__avg"], 0)
        app_amount = round(Application.objects.all().aggregate(Avg("amount"))["amount__avg"], 0)
        student_app_amount = round(Student_application.objects.all().aggregate(Avg("amount"))["amount__avg"], 0)

        last_month = timezone.now().date() - timedelta(days=30)

        student_create = Student.objects.filter(
            created_date__lt = timezone.now(),
            created_date__gte = last_month,

        ).annotate(
            day=ExtractDay('created_date'),
        ).values(
            'day'
        ).annotate(
            n=Count('pk')
        ).order_by('day')


        app_create = Application.objects.filter(
            created_date__lt = timezone.now(),
            created_date__gte = last_month,

        ).annotate(
            day=ExtractDay('created_date'),
        ).values(
            'day'
        ).annotate(
            n=Count('pk')
        ).order_by('day')

        data = {
            "paid":student_app_amount,
            "not_paid":student_contract - student_balance,
            "asked":student_contract,
            "balance":app_amount,
            "sponsor":app_create,
            "student":student_create,
        }

        return Response(data)

class StudentDetailUpdate(APIView):

    def get(self, request, pk):
        student = Student.objects.get(id = pk)
        serializer = StudentSerializer(student, many = False)
        return Response(serializer.data)

    def patch(self, request, pk):
        student = Student.objects.get(id = pk)
        serializer = StudentSerializer(instance = student, data = request.data, partial = True)
        if serializer.is_valid():
            serializer.save()

        return Response(serializer.data)


class ApplicationDetailUpdate(APIView):

    def get(self, request, pk):
        app = Application.objects.get(id = pk)
        serializer = ApplicationListCreateSerializer(app, many = False)
        return Response (serializer.data)

    def patch(self, request, pk):
        app = Application.objects.get(id = pk)
        serializer = ApplicationListCreateSerializer(instance = app, data = request.data, partial = True)
        if serializer.is_valid():
            serializer.save()

        return Response(serializer.data)

class StudentApplicationDetailUpdate(APIView):

    def get(self, request, pk):
        app = Student_application.objects.get(id = pk)
        serializer = StudentApplicationListSerializer(app, many = False)
        return Response (serializer.data)

    def patch(self, request, pk):
        app = Student_application.objects.get(id = pk)
        serializer = StudentApplicationSerializer(instance = app, data = request.data, partial = True)
        if serializer.is_valid():
            serializer.save()

        return Response(serializer.data)