from dataclasses import field
from .models import (
    Student, Application, Student_application
)
from django_filters import rest_framework as filters

class StudentFilter(filters.FilterSet):

    class Meta:
        model = Student
        fields = ['degree', 'otm']

class ApplicationFilter(filters.FilterSet):

    class Meta:
        model = Application
        fields = [
            "full_name",
            "phone",
            "amount",
            "is_juridical",
            "organization",
            "created_date",
        ]

class StudentAppFilter(filters.FilterSet):
    application = filters.CharFilter(field_name="application__full_name", lookup_expr='icontains')
    student = filters.CharFilter(field_name="application__full_name", lookup_expr='icontains')
    
    class Meta:
        model = Student_application
        fields = [
            "application",
            "student",
            "amount",
            "status",
            "created_date",
        ]